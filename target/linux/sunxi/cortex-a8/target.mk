#
# Copyright (C) 2013 OpenWrt.org
#

BOARDNAME:=A10, A10s and A13
SUBTARGET:=cortex-a8
CPU_TYPE:=cortex-a8

define Target/Description
 Allwinner A10, A10s and A13 SoC
endef
