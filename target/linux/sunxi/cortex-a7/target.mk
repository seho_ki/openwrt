#
# Copyright (C) 2013 OpenWrt.org
#

BOARDNAME:=A20, A31 and A31s
SUBTARGET:=cortex-a7
CPU_TYPE:=cortex-a7

define Target/Description
 Allwinner A20, A31 and A31s SoC
endef
